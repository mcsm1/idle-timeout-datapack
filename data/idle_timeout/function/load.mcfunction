scoreboard objectives add it_clock dummy
scoreboard objectives add it_players dummy
scoreboard objectives add it_max dummy
scoreboard objectives add it_calc dummy

scoreboard players set #ItHandler it_clock 0
scoreboard players set #ItHandler it_players 0
execute unless score #ItHandler it_max matches 0.. run scoreboard players set #ItHandler it_max 30

schedule function idle_timeout:check 30s
