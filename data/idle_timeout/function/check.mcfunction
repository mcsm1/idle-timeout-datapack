execute store result score #ItHandler it_players if entity @a

execute if score #ItHandler it_players matches 1.. run scoreboard players set #ItHandler it_clock 0
execute if score #ItHandler it_players matches 0 run scoreboard players add #ItHandler it_clock 1

scoreboard players set #ItHandler it_calc 2
scoreboard players operation #ItHandler it_calc *= #ItHandler it_max
scoreboard players operation #ItHandler it_calc -= #ItHandler it_clock

execute unless score #ItHandler it_calc matches 1.. run stop

schedule function idle_timeout:check 30s
