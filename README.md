# David's Idle Timeout Datapack
The purpose of this datapack is to automatically shut down a Minecraft server
after all players have logged off.

I suspect this will not break on updates; however, you may always
[create an issue](https://gitlab.com/mcsm1/idle-timeout-datapack/-/issues/new).

Note, this is for java edition only.

## MCSM
This is one component of my [Minecraft Server Manager](https://gitlab.com/mcsm1)
tools. It can be used independently, but check out the other tools, if you wish.

## Installation
Start by ensuring that `function-permission-level` is set to `4` in your
`server.properties` file. Without this, the datapack cannot run the `stop`
command needed to stop the server. Note that this will apply to ALL datapacks.

Download the
[latest release](https://gitlab.com/mcsm1/idle-timeout-datapack/-/releases).
Then, place this into your world's datapack folder.
Check out the [wiki](https://minecraft.wiki/w/Tutorials/Installing_a_data_pack)
for more help.
Once installed, run `/reload` to load the new datapack.
If you changed the `server.properties` file, you may need to restart the server.

## Customization
By default, the server will continue to run for 30 minutes after all players
exit. Note that the datapack only checks for players every 30 seconds, so the
timing may not be exact.

To change this time, run `/scoreboard players set #ItHandler it_max NUM` where
`NUM` is the number of minutes you wish the server to remain on after all
players exit.

## Uninstallation
Run `/function idle_timeout:uninstall`.
Promptly remove the datapack from the world folder.
Lower the `function-permission-level`, if you wish.

If you run `/reload` or restart the server after running the uninstall, but
before removing the folder, you will need to start again to fully remove it.
